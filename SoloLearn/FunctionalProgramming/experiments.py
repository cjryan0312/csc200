from itertools import *
def factorial(x):
    #1 is the base case, 1! will always return 1 because 1
    if x == 1:
        return 1
    #keeps calling factorial function over and over again, until x hits 1
    else:
        return x * factorial(x-1)

#returns 120
print(factorial(5))

#incorrect fact func:
#def inc_factorial(x):
    #this is where a base case should be, but no such thing exists
    #return x * factorial(x-1)
#this will return a RuntimeError because infinite loop if called


def is_even(y):
    if y == 0:
        return True
    else:
        return is_odd(y - 1)

def is_odd(y):
    return not is_even(y)

mySet = {1, 'yo', 'h'}
truth = 1 in mySet)
truthnt = "AHHHHHHHH" in mySet

mySet.add("cheeseburbger")
mySet.remove(1)

jebaited = {'you', 'got', 'jebaited', 'h'}
union = mySet | jebaited    #appends together
intersection = mySet & jebaited     #everything that is same in both sets shown
difference = mySet - jebaited   #items that are in first set but not the second
symmetric = mySet ^ jebaited      #item in either but not in both

emptySet = set() #to make an empty set you have to do this 

counting = ''
for i in count(3):
    counting += i
    if i >= 11:
        break
numbas = list(accumulate(range(8))) # all numbers not divisible by 8
listing = list(takewhile(lambda x: x<=6, numbas))

lemterms: ('a', 'b')
productive = list(product(letters, range(1)))  #possible locations of iterable in range[('a', 0), ('b', 0)]
permutative = list(permutations(lemterms))   #all possible sorting combos in iterable

def fib(n):
    if n == 0 or n == 1:
        return 1
    return fib(n-1) + fib(n-2)









