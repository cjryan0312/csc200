#hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh
#attributes are very important 
#methods 
#self, self is the name of the instance being created 
class dog:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def sayHi(self):
        print(f'Hi my name is {self.name} and my age is {self.age}')


dog = dog('fido', 5)
dog.sayHi()

class Student:
    grade = "sophmore"
    def __init__(self, name):
        self.name = name
        
guy = Student("guy")
Student.grade

class Animal:
    def __init__(self, name, color):
        self.name = name
        self.color = color
    def hop(self):
        print("this will be overrided") #this will be overrided
    
    def goodBoy(self):
        print("whose a good boy?")

class Rabbit(Animal):
    def hop(self):
        print("hop")
        super().hop()

    def gooderBoy(self):
        print("whose a good boy?")

class Cat(Rabbit):
    def goodestBoy(self):
        print("whose a good boy?")

class dd:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def __add__(self, other):
         return dd(self.x + other.x, self.y + other.y)

class testing:
    def __init__(self):
        self.me = 5
        
    def __add__(self):
        rresult = self.me + 5


first = dd(5, 7)
second = dd(3, 9)
result = first + second
print(result.x)
print(result.y)

answer = testing()
print(answer.yes())
